const { expect } = require('chai');
const { createSandbox } = require('sinon');
const PaymentService = require('../../src/services/PaymentService');
const { ACCEPTED, REJECTED } = require('../../src/utils/paymentConstants');

describe('PaymentService', () => {
    let sandbox;
    let mockAxios;
    let paymentService;

    beforeEach(() => {
        sandbox = createSandbox();

        mockAxios = { post: sandbox.stub() };

        paymentService = new PaymentService(mockAxios);
    });
    
    describe('authorizePayment()', () => {
        it('deve retornar false se o pagamento for rejeitado', async () => {
            mockAxios.post.resolves({
                status: 200,
                data: {
                    estado: REJECTED
                }
            });

            const authorized = await paymentService.authorizePayment({});

            expect(authorized).to.be.false;
        });

        it('deve retornar true se o pagamento for aceito', async () => {
            mockAxios.post.resolves({
                status: 200,
                data: {
                    estado: ACCEPTED
                }
            });

            const authorized = await paymentService.authorizePayment({});

            expect(authorized).to.be.true;
        });
    });
});