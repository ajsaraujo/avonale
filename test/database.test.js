const { expect } = require('chai');
const { createSandbox } = require('sinon');
const Database = require('../src/database');

describe('Database', () => {
    let sandbox;
    let mockMongoose;
    let database;
    
    beforeEach(() => {
        sandbox = createSandbox();

        mockMongoose = {
            connect: sandbox.stub(),
            connection: {
                close: sandbox.stub()
            }
        };

        database = new Database(mockMongoose);
    });

    describe('connect()', () => {
        it('deve se conectar ao mongoose', async () => {
            await database.connect();

            expect(mockMongoose.connect.calledOnce).to.be.true;
        });
    });

    describe('close()', () => {
        it('deve fechar a conexão', async () => {
            await database.close();

            expect(mockMongoose.connection.close.calledOnce).to.be.true;
        });
    });
});

