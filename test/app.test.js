const { expect } = require('chai');
const { createSandbox } = require('sinon');

const createApp = require('../src/app');
const Database = require('../src/database');

describe('app', () => {
    let sandbox;
    let mockExpressFunction;
    let mockExpressInstance;
    let mockMongoose;
    let mockMiddlewares;

    beforeEach(() => {
        sandbox = createSandbox();
    
        mockExpressInstance = {
            use: sandbox.stub(),
            get: sandbox.stub(),
            listen: sandbox.stub()
        };

        mockMongoose = {};

        mockExpressFunction = () => mockExpressInstance;

        mockMiddlewares = [sandbox.stub(), sandbox.stub()]
    });

    it('retorna um objeto com um database', async () => {
        const app = await createApp(mockExpressFunction, mockMongoose);

        expect(app.database instanceof Database).to.be.true;
        expect(app.database.mongoose).to.equal(mockMongoose);
    });

    it('usa os middlewares fornecidos', async () => {
        const app = await createApp(mockExpressFunction, mockMongoose, mockMiddlewares);

        mockMiddlewares.forEach(middleware => {
            expect(app.use.calledWith(middleware)).to.be.true;
        });
    });
});