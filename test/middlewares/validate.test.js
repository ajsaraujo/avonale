const { expect } = require('chai');
const { createSandbox } = require('sinon');
const Mocks = require('../Mocks');
const validate = require('../../src/middlewares/validate');

describe('validate', () => {
    let sandbox;
    let mockValidator;
    let req;
    let res;
    let next;

    beforeEach(() => {
        sandbox = createSandbox();

        mockValidator = {
            validateAsync: sandbox.stub()
        };

        req = Mocks.req();
        res = Mocks.res();
        next = Mocks.next(sandbox);
    });

    it('deve retornar 412 se a validação lançar erro', async () => {
        mockValidator.validateAsync.throws(new Error('Dados inválidos'));

        const { status, json } = await validate(mockValidator)(req, res, next);

        expect(status).to.equal(412);
        expect(json.message).to.equal('Os valores informados não são válidos.');
    });

    it('deve chamar next se estiver tudo ok', async () => {
        await validate(mockValidator)(req, res, next);

        expect(next.calledOnce).to.be.true;
    });
});