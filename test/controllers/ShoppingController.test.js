const { expect } = require('chai');
const { createSandbox } = require('sinon');
const Mocks = require('../Mocks');

const ShoppingController = require('../../src/controllers/ShoppingController');

describe('ShoppingController', () => {
    let sandbox;
    let shoppingController;
    let mockProduct;
    let mockPaymentService;
    let req;
    let res;

    beforeEach(() => {
        sandbox = createSandbox();

        mockProduct = { findByIdInDetail: sandbox.stub(), sellOne: sandbox.stub() };
        mockPaymentService = { authorizePayment: sandbox.stub() };

        shoppingController = new ShoppingController(mockProduct, mockPaymentService);

        req = Mocks.req();
        res = Mocks.res();

        req.body = {
            produto_id: '123456789'
        };
    });
    
    describe('buy', () => {
        it('retorna 404 se não existe produto referente ao id', async () => {
            mockProduct.findByIdInDetail.resolves(null);

            const { status, json } = await shoppingController.buy(req, res);

            expect(status).to.equal(404);
            expect(json.message).to.equal('Produto não encontrado.');
        });

        it('retorna 412 se não tem unidades suficientes no estoque', async () => {
            mockProduct.findByIdInDetail.resolves({
                qtde_estoque: 1
            });
            
            req.body.qtde_comprada = 3;

            const { status, json } = await shoppingController.buy(req, res);

            expect(status).to.equal(412);
            expect(json.message).to.equal('Tentou comprar 3 unidades, mas tem apenas 1 em estoque.');
        });

        it('retorna 412 se o item está esgotado', async () => {
            mockProduct.findByIdInDetail.resolves({
                qtde_estoque: 0
            });

            const { status, json } = await shoppingController.buy(req, res);

            expect(status).to.equal(412);
            expect(json.message).to.equal('Produto esgotado.');
        });

        it('retorna 400 se houver um erro desconhecido', async () => {
            mockProduct.findByIdInDetail.throws(new Error('Falhou!'));

            mockPaymentService.authorizePayment.returns(false);

            const { status, json } = await shoppingController.buy(req, res);

            expect(status).to.equal(400);
            expect(json.message).to.equal('Ocorreu um erro desconhecido.');
        });

        it('retorna 412 se a API de pagamento não autorizar a compra', async () => {
            mockProduct.findByIdInDetail.resolves({
                qtde_estoque: 1
            });

            mockPaymentService.authorizePayment.returns(false);

            const { status, json } = await shoppingController.buy(req, res);

            expect(status).to.equal(412);
            expect(json.message).to.equal('Pagamento não autorizado.');
        });

        it('retorna 200 se der tudo certo e dá baixa no item', async () => {
            const sellStub = sandbox.stub();
            
            mockPaymentService.authorizePayment.returns(true);

            mockProduct.findByIdInDetail.resolves({
                qtde_estoque: 1,
                sell: sellStub
            });

            const { status, json } = await shoppingController.buy(req, res);

            expect(status).to.equal(200);
            expect(json.message).to.equal('Compra concluída com sucesso.');
            expect(sellStub.calledOnce);
        });
    });
});