const { expect } = require('chai');
const Mocks = require('../Mocks');
const PaymentController = require('../../src/controllers/PaymentController');
const { ACCEPTED, REJECTED } = require('../../src/utils/paymentConstants');

describe('PaymentController', () => {
    let req;
    let res;
    let paymentController;
    let mockPayment = {
        cartao: {
            titular: 'Homer Souza',
            numero: '1111' + '1111' + '1111' + '1111',
            data_expiracao: '12/2021',
            cvv: '111'
        }
    }

    beforeEach(() => {
        paymentController = new PaymentController();

        req = Mocks.req();
        res = Mocks.res();

        req.body = mockPayment;
    });

    describe('authorizePayment()', () => {
        it('deve rejeitar se o valor da venda for menor do que 100', () => {
            req.body.valor = 99.00;

            const { status, json } = paymentController.authorizePayment(req, res);

            expect(status).to.equal(200);
            expect(json).to.eql({ valor: 99.00, estado: REJECTED });
        });

        it('deve aceitar se o valor da venda for maior ou igual a 100', () => {
            req.body.valor = 101.00;

            const { status, json } = paymentController.authorizePayment(req, res);

            expect(status).to.equal(200);
            expect(json).to.eql({ valor: 101.00, estado: ACCEPTED });
        });
    });
});