const { expect } = require('chai');
const { createSandbox } = require('sinon');
const Mocks = require('../Mocks');
const ProductController = require('../../src/controllers/ProductController');

describe('ProductController', () => {
    let sandbox;
    let mockProduct;
    let productController;

    let req;
    let res;

    beforeEach(() => {
        sandbox = createSandbox();

        mockProduct = {
            create: sandbox.stub(),
            find: sandbox.stub(),
            findByIdAndDelete: sandbox.stub(),
            findByIdInDetail: sandbox.stub()
        };

        productController = new ProductController(mockProduct);

        req = Mocks.req();
        res = Mocks.res();

        req.body = {
            nome: 'Rebimboca da Parafuseta',
            valor_unitario: 1.00,
            qtde_estoque: 1000
        };
    });

    describe('create()', () => {
        it('deve retornar 400 quando houver um erro desconhecido', async () => {
            mockProduct.create.throws(new Error('Erro ao criar produto'));
            
            const { status, json } = await productController.create(req, res);

            expect(status).to.equal(400);
            expect(json.message).to.equal('Ocorreu um erro desconhecido.');
        });

        it('deve retornar 200 quando o produto for cadastrado', async () => {
            const { status, json } = await productController.create(req, res);

            expect(mockProduct.create.calledWith(req.body)).to.be.true;
            expect(status).to.equal(200);
            expect(json.message).to.equal('Produto cadastrado.');
        });
    });

    describe('getAll()', () => {
        it('deve retornar 400 quando houver um erro desconhecido', async () => {
            mockProduct.find.throws(new Error('Erro ao buscar produtos'));

            const { status, json } = await productController.getAll(req, res);

            expect(status).to.equal(400);
            expect(json.message).to.equal('Ocorreu um erro desconhecido.');
        });

        it('deve retornar 200 e uma lista de produtos', async () => {
            const products = [
                { nome: 'Gel Capilar Garoto Pimpão 229mL', qtde_estoque: 100, valor_unitario: 22.50 },
                { nome: 'Bolsa Kari Ssima', qtde_estoque: 5, valor_unitario: 799.99 },
                { nome: 'Playstation 5 Golden Edition', qtde_estoque: 1, valor_unitario: 9999.99 }
            ];
            
            mockProduct.find.resolves(products);

            const { status, json } = await productController.getAll(req, res);

            expect(status).to.equal(200);
            expect(json).to.eql(products);
        });
    });

    describe('getById()', () => {
        it('deve retornar 400 se ocorrer um erro desconhecido', async () => {
            mockProduct.findByIdInDetail.throws(new Error('Erro ao buscar produto'));

            const { status, json } = await productController.getById(req, res);

            expect(status).to.equal(400);
            expect(json.message).to.equal('Ocorreu um erro desconhecido.');
        });

        it('deve retornar 404 se o produto não for encontrado', async () => {
            mockProduct.findByIdInDetail.resolves(null);

            const { status, json } = await productController.getById(req, res);

            expect(status).to.equal(404);
            expect(json.message).to.equal('Produto não encontrado.');
        });

        it('deve retornar 200 e o produto', async () => {
            const product = {
                nome: 'A Metamorfose - Franz Kafka L&PM Pocket',
                qtde_estoque: 75,
                valor_unitario: 19.74
            };

            mockProduct.findByIdInDetail.resolves(product);

            const { status, json } = await productController.getById(req, res);

            expect(status).to.equal(200);
            expect(json).to.eql(product);
        });
    });

    describe('delete()', async () => {
        it('deve retornar 400 quando houver um erro desconhecido', async () => {
            mockProduct.findByIdAndDelete.throws(new Error('Erro desconhecido'));

            const { status, json } = await productController.delete(req, res);

            expect(status).to.equal(400);
            expect(json.message).to.equal('Ocorreu um erro desconhecido.');
        });

        it('deve retornar 404 se o produto não for encontrado', async () => {
            mockProduct.findByIdAndDelete.resolves(null);

            const { status, json } = await productController.delete(req, res);

            expect(status).to.equal(404);
            expect(json.message).to.equal('Produto não encontrado.');
        });

        it('deve retornar 200 e deletar o produto', async () => {
            const product = {
                nome: 'Criterion Collection\'s 12 Angry Men Blu-ray Edition',
                qtde_estoque: 6,
                valor_unitario: 39.82
            };
            
            mockProduct.findByIdAndDelete.resolves(product);

            const { status, json } = await productController.delete(req, res);

            expect(status).to.equal(200);
            expect(json.message).to.equal('Produto excluído com sucesso.');
        });
    });

    afterEach(() => {
        sandbox.restore();
    });
})