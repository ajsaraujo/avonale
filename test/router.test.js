const { expect } = require('chai');
const { createSandbox } = require('sinon');
const createRouter = require('../src/router');

describe('router', () => {
    let sandbox;
    let mockRouter;
    let mockFileUtils;
    let fakeRoutes;

    beforeEach(() => {
        sandbox = createSandbox();
        
        mockRouter = {
            post: sandbox.stub(),
            get: sandbox.stub(),
            put: sandbox.stub(),
            delete: sandbox.stub()
        };
        
        mockFileUtils = { getFiles: sandbox.stub() };

        fakeRoutes = [sandbox.stub(), sandbox.stub()];

        mockFileUtils.getFiles.resolves(fakeRoutes);
    });

    it('deve obter as rotas de routes/', async () => {
        await createRouter(mockRouter, mockFileUtils);

        expect(mockFileUtils.getFiles.calledOnce).to.be.true;
    });

    it('deve usar as rotas obtidas', async () => {
        await createRouter(mockRouter, mockFileUtils);

        fakeRoutes.forEach(routeFunction => {
            expect(routeFunction.calledOnce).to.be.true;
        });
    });

    afterEach(() => {
        sandbox.restore();
    });
});