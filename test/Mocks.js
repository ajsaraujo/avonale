function req() {
    return {
        body: {},
        params: {},
        headers: {}
    }
};

function res() {
    const Response = class Response {
        status(statusCode) {
            this.status = statusCode;
            return this;
        }

        json(data) {
            this.json = data;
            return this;
        }
    };

    return new Response();
}

function next(sandbox) {
    return sandbox.stub();
}

module.exports = {
    req,
    res,
    next
}