# API de Compras

Esta é minha implementação para o desafio técnico proposto. Aqui vou explicar
como executar o projeto, e também o que motivou algumas de minhas decisões. Espero que gostem! 😀

## Pré-requisitos

Para executar o App, você precisará de:

- [Git](https://git-scm.com/)
- [MongoDB](https://www.mongodb.com/)
- [Node.js](https://nodejs.org/en/)

Você pode rodar uma instância do MongoDB na sua máquina, ou utilizar a tier gratuita do MongoAtlas, o serviço de banco de dados na nuvem do MongoDB. Não quer configurar nada disso? A aplicação está hospedada no Heroku! Clique [aqui](https://avonale-challenge.herokuapp.com/api/) para visitá-la. Note que o Heroku põe a aplicação pra dormir após um período de inatividade, então você pode experimentar um delay suave na primeira requisição que você fizer (o aplicativo estará iniciando). Depois disso, o app voltará ao normal.

## Instalação e Uso

Primeiro, baixamos o projeto e instalamos as dependências.
```sh
# Baixando o projeto
$ git clone https://bitbucket.org/ajsaraujo/avonale.git

$ cd avonale/

# Instalando as dependências
$ npm i
```

Depois, é preciso configurar as variáveis de ambiente. Na pasta raiz, há um arquivo `.env.example` exemplificando quais são as variáveis de ambiente. Ainda na pasta raiz, crie um arquivo chamado `.env`, cole nele o conteúdo de `.env.example` e certifique-se de que todos os valores estão corretamente preenchidos. 

- `PORT` -> porta em que a aplicação ficará hospedada.
- `DB_URI` -> string de conexão com o banco de dados.
- `PAYMENT_API_HOST` -> URL da API de pagamento. Será a mesma URL da aplicação: `localhost:<PORT>/api`

Feito isso, para executar a aplicação basta rodar `npm start`. Para rodar os testes, use `npm test`. 

Após alguns instantes, você deverá observar uma saída similar a esta:
```
$ npm start

> desafio-avonale@1.0.0 start <caminho>\desafio-avonale
> node src/index.js

Sucessfully connected to the database!
App is listening on port <PORT>!
```

Feito isso, você poderá visualizar a documentação da API visitando `localhost:<PORT>/api` com o seu navegador favorito.
Lembrando que a aplicação também está hospedada em `https://avonale-challenge.herokuapp.com/api/`.

## Estrutura do projeto

`controllers/`

Em `controllers/`, estão os controladores, que implementam a lógica de negócio, definindo como a aplicação responderá a cada requisição.

`middlewares/`

Em `middlewares/`, estão rotinas que serão reutilizadas em diferentes endpoints. Como o escopo da aplicação é reduzido, só foi necessário um middleware de validação de dados, que garante que os dados sempre chegarão limpos para os controladores. Em aplicações maiores, certamente também haveriam middlewares de autenticação, autorização e outras funções.

`models/`

Em `models/`, estão os modelos de dados do app, que definem como as informações serão salvas no banco de dados. Aqui também são definidos os modelos de validação de dados usando a lib `joi`, que serão passados para o middleware de validação. Os models também definem alguns métodos sobre aqueles objetos. Por exemplo, o `Product` define o método `findByIdInDetail()`, que retorna o produto com seus detalhes. Fazendo isso eu aumento o isolamento e facilito os testes. 

`routes/`

Em `routes/`, estão as rotinas responsáveis por acoplar os métodos dos controladores a seus respectivos endpoints.

`services/ e utils/`

Em `services/` e `utils/` estão alguns utilitários. Escolhi separar `PaymentService` de `utils/` pois creio que seu escopo seja ligeiramente diferente, ele não está expondo exatamente um método utilitário, e sim consumindo uma API de terceiros. 

### Arquivos de Inicialização

- `database.js`: define as rotinas de estabelecimento e encerramento de conexão com o banco de dados.
- `router.js`: itera pelos arquivos de `routes/` para compor o `Router` da aplicação. Ele também acopla os controladores aos seus respectivos models.
- `app.js`: instancia a aplicação, injetando middlewares, banco de dados e roteador.
- `index.js`: onde tudo começa. Cria o app, conecta-o ao banco de dados, coloca-o de pé e por fim o prepara para fechar graciosamente quando necessário.

## Considerações

### Injeção de Dependências

Utilizei injeção de dependências por todo o projeto. Facilita muito na hora de fazer os testes unitários. Apesar de no JavaScript existirem libs que permitem você mockar suas dependências com certa facilidade, como o Sinon, eventualmente você acaba se deparando com comportamentos estranhos. Com injeção de dependências você evita isso, é um padrão que gosto de usar. 

### SRP no Router

O `router.js` fere um pouco o SRP, já que ele é responsável tanto por acoplar `Controller` e `Model` como por formar o roteador. Como a aplicação é bem pequena isso acaba não doendo tanto, mas eventualmente esse arquivo poderia crescer mais do que o desejado. Para evitar isso, uma solução seria mover esse acoplamento para um arquivo separado, que já exportaria esses objetos contendo os controllers. 

Outra coisa que você pode se perguntar é o porquê de injetar os middlewares, ao invés de simplesmente requerí-los como faço com os models. O que acontece é que eventualmente, os middlewares, assim como os controllers, possam requerir uma lógica de acoplamento. Imagine que eu tenha um middleware de autenticação com JWT. Certamente vou querer injetar nesse middleware um objeto que criptografa/descriptografa informações para JWT. Outro exemplo é um middleware que consulta o banco de dados. Com isso os middlewares são instanciados no acoplador e passados como argumento, prontinhos para usar, para os arquivos de rota.

### Transação no BD

Fiquei preocupado se a concorrência na aplicação não geraria comportamentos estranhos ao dar baixa no produto. Imagine, por exemplo, que dois usuários compram um mesmo produto num intervalo de tempo curto, causando uma condição de corrida. Implementar transações em bancos relacionais é bem tranquilo, mas, no MongoDB, e em especial com Mongoose (o ORM que estou usando), ainda não descobri como fazer. É algo que fico devendo, pretendo fazer um curso de MongoDB em breve para sanar dúvidas desse tipo.

### Nomeando Variáveis

O styleguide que sigo nos meus projetos em Node é o da Airbnb, de modo que nomeio minhas variáveis em 
CamelCase. Além disso, eu também prefiro nomear tudo em inglês, acho que em português acaba ficando um pouco
destoante, já que as palavras chave são todas em inglês. Porém, na instrução do desafio, vocês passaram os objetos em snake_case *e* em português, ou seja, totalmente diferente do que uso! Pensei em fazer o projeto todo em snake_case e em português, mas achei que ficaria um pouco estranho e acabei optando por fazer como já estou acostumado. No final ficou estranho do mesmo jeito, com direito a bilinguismos como este logo abaixo. Agora já foi! Espero que não se incomodem com isso.

```js
const payment = {
    valor: price * numOfItemsBought,
    cartao: req.body.cartao
};
```

### Duas APIs

A especificação do problema deu a entender que as APIs de compra e pagamento deveriam estar fisicamente separadas, isto é, em aplicações diferentes. Faz todo o sentido, já que uma vai fazer requisições para a outra. Creio que vocês queriam avaliar como o candidato lida com microsserviços. Por comodidade, acabei implementando tudo numa coisa só, mas acredito que não haja muito prejuízo nisso, levando em conta o escopo desse projeto. 

Se eu fosse fazer as duas APIs diferentes, o maior problema seria como lidar com a lógica que é comum a ambas. Por exemplo, ambas as aplicações precisariam das rotinas de inicialização (`app`, `database`, `router` e `index`), além de `Models` como `Product`, e outros de validação, como `CreditCard` e `Payment`. A solução mais inocente seria simplesmente copiar e colar esses arquivos em ambos os projetos, mas é óbvio que rapidamente a manutenção disso se tornaria infernal. Creio que uma boa forma de lidar com esse problema seria publicar um pacote privado no NPM, contendo toda a lógica comum. Onde fosse necessário, bastaria fazer algo do tipo `const avonale = require('avonale')` e obteria os objetos relevantes. Dessa forma a lógica comum ficaria em um único repositório, dispensando a replicação do código pelos repositórios clientes. Para obter versões mais atualizadas bastaria uma alteração no `package.json` e um `npm i`. 

### Respostas da API de pagamento

A API de pagamento retorna 200 tanto quando o pagamento é autorizado como quando não é. Para o cliente saber se o pagamento foi autorizado ele deve checar a propriedade `estado` no JSON retornado. Entendo que deve ser assim, pois o 200 representa que tudo na requisição correu bem. Eu não poderia retornar um 400, por exemplo, indicando que aquela foi uma requisição ruim - os dados da requisição estão coerentes e foram corretamente tratados pela API, o feedback negativo é da lógica de negócio, e não do sistema. 