const Database = require('./database');
const createRouter = require('./router');
const fileUtils = require('./utils/fileUtils');

async function injectMiddlewares(app, middlewares) {
    middlewares.forEach(middleware => {
        app.use(middleware);
    });
}

async function injectRouter(app, express) {
    const router = await createRouter(express.Router(), fileUtils);
    
    app.use('/api', router);
}

async function createApp(express, mongoose, middlewares) {
    const app = express();
    
    app.database = new Database(mongoose);

    injectMiddlewares(app, middlewares);
    injectRouter(app, express);

    return app;
}

module.exports = createApp;