const { ACCEPTED } = require('../utils/paymentConstants');

class PaymentService {
    constructor(axios) {
        this.axios = axios;
    }

    async authorizePayment(payment) {
        const url = process.env.PAYMENT_API_HOST + '/pagamento/compras';

        try {
            const { data } = await this.axios.post(url, payment);
            return data.estado === ACCEPTED;
        } catch (err) {
            console.log(err.message);
            return false;
        }
    }
}

module.exports = PaymentService;