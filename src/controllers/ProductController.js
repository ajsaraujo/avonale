class ProductController {
    constructor(Product) {
        this.Product = Product;
    }

    async getAll(req, res) {
        try {
            const products = await this.Product.find({});
            return res.status(200).json(products);
        } catch (error) {
            return res.status(400).json({ message: 'Ocorreu um erro desconhecido.' });
        }
    }

    async getById(req, res) {
        try {    
            const product = await this.Product.findByIdInDetail(req.params.id);
            
            if (!product) {
                return res.status(404).json({ message: 'Produto não encontrado.'})
            }
            
            return res.status(200).json(product);
        } catch (error) {
            return res.status(400).json({ message: 'Ocorreu um erro desconhecido.' });
        }
    }

    async create(req, res) {
        try {
            await this.Product.create(req.body);
            return res.status(200).json({ message: 'Produto cadastrado.'});
        } catch (error) {
            return res.status(400).json({ message: 'Ocorreu um erro desconhecido.' });
        }
    }

    async delete(req, res) {
        try {

            const product = await this.Product.findByIdAndDelete(req.params.id);
            
            if (!product) {
                return res.status(404).json({ message: 'Produto não encontrado.' });
            }
            
            return res.status(200).json({ message: 'Produto excluído com sucesso.' });
        } catch (error) {
            return res.status(400).json({ message: 'Ocorreu um erro desconhecido.' });
        }
    }
}

module.exports = ProductController;