class ShoppingController {
    constructor(Product, paymentService) {
        this.Product = Product;
        this.paymentService = paymentService;
    }

    async buy(req, res, next) {
        try {
            const productId = req.body.produto_id;
            const product = await this.Product.findByIdInDetail(productId);
            
            if (!product) {
                return res.status(404).json({ message: 'Produto não encontrado.' });
            }
            
            const price = product.valor_unitario;
            const numOfStockedItems = product.qtde_estoque;
            const numOfItemsBought = req.body.qtde_comprada; 
            
            if (numOfStockedItems <= 0) {
                return res.status(412).json({ message: 'Produto esgotado.' });
            }

            if (numOfStockedItems < numOfItemsBought) {
                return res.status(412).json({ message: `Tentou comprar ${numOfItemsBought} unidades, mas tem apenas ${numOfStockedItems} em estoque.`})
            }

            const payment = {
                valor: price * numOfItemsBought,
                cartao: req.body.cartao
            };

            const authorized = await this.paymentService.authorizePayment(payment);

            if (!authorized) {
                return res.status(412).json({ message: 'Pagamento não autorizado.' });
            }

            await product.sell(numOfItemsBought);

            return res.status(200).json({ message: 'Compra concluída com sucesso.' });
        } catch (error) {
            return res.status(400).json({ message: 'Ocorreu um erro desconhecido.', error });
        }
    }
}

module.exports = ShoppingController;