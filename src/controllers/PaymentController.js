const { ACCEPTED, REJECTED } = require('../utils/paymentConstants');

class PaymentController {
    authorizePayment(req, res, next) {
        const { valor } = req.body;
        const estado = valor < 100 ? REJECTED : ACCEPTED;

        return res.status(200).json({ valor, estado });
    }
}

module.exports = PaymentController;