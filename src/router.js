const axios = require('axios');
const path = require('path');

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../swagger.json');

const ProductController = require('./controllers/ProductController');
const PaymentController = require('./controllers/PaymentController');
const ShoppingController = require('./controllers/ShoppingController');

const PaymentService = require('../src/services/PaymentService');
const Product = require('./models/Product');
const validate = require('./middlewares/validate');

function createControllers() {
    const paymentService = new PaymentService(axios);

    return {
        productController: new ProductController(Product),
        paymentController: new PaymentController(),
        shoppingController: new ShoppingController(Product, paymentService)
    }
}

function createMiddlewares() {
    return {
        validate
    }
}

async function injectRoutes(router, fileUtils, controllers, middlewares) {
    const routesDirectory = path.join(__dirname, 'routes');
    const routes = await fileUtils.getFiles(routesDirectory, 'Route.js');
    
    routes.forEach(addRouteFunction => {
        addRouteFunction(router, controllers, middlewares);
    });
}

async function createRouter(router, fileUtils) {
    router.use('/', swaggerUi.serve);
    router.get('/', swaggerUi.setup(swaggerDocument));
    
    const controllers = createControllers();
    const middlewares = createMiddlewares();
    
    await injectRoutes(router, fileUtils, controllers, middlewares);

    return router;
}

module.exports = createRouter;