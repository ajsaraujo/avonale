const Joi = require('joi');
const CreditCard = require('./CreditCard');

module.exports = {
    joiValidator: Joi.object({
        produto_id: Joi.string().required(),
        qtde_comprada: Joi.number().integer().min(1).required(),
        cartao: CreditCard.joiValidator.required()
    })
}