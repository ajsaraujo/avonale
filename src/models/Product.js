const { Schema, model } = require('mongoose');
const Joi = require('joi');

const ProductSchema = new Schema({
    nome: {
        type: String,
        required: true
    },

    valor_unitario: {
        type: Number,
        required: true
    },

    qtde_estoque: {
        type: Number,
        required: true
    },

    ultima_venda: {
        type: Date,
        select: false,
        default: null
    }
});


ProductSchema.statics.findByIdInDetail = async function findByIdInDetail(id) {
    const product = await this.findById(id).select('+ultima_venda');

    return product;
};

ProductSchema.methods.sell = async function sell(quantity) {
    this.ultima_venda = Date.now();
    this.qtde_estoque -= quantity;
    
    await this.save();
}

const Product = model('Product', ProductSchema);

Product.joiValidator = Joi.object({
    nome: Joi.string().required().max(250),
    valor_unitario: Joi.number().required().min(0),
    qtde_estoque: Joi.number().required().integer().min(0)
});

module.exports = Product;