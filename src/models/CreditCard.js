const Joi = require('joi');

const cardNumber = new RegExp(/^[0-9]{16}$/);

const expiryDateValidator =  (value) => {
    const [monthString, yearString] = value.split('/');

    const month = Number(monthString);
    const year = Number(yearString);

    if (month === NaN || year === NaN || value[2] !== '/') {
        throw new Error('Data de expiração deve estar no formato mm/yyyy');
    }

    if (month < 1 || month > 12 || year < 0) {
        throw new Error('Data inválida');
    }

    return value;
};

const cvv = new RegExp(/^[0-9]{3}$/);

module.exports = {
    joiValidator: Joi.object({
        titular: Joi.string().required().max(500),
        numero: Joi.string().regex(cardNumber),
        data_expiracao: Joi.string().required().length(7).custom(expiryDateValidator),
        bandeira: Joi.string().required(),
        cvv: Joi.string().required().regex(cvv)
    })
};