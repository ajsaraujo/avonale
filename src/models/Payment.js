const Joi = require('joi');
const CreditCard = require('./CreditCard');

module.exports = {
    joiValidator: Joi.object({
        valor: Joi.number().required().min(0),
        cartao: CreditCard.joiValidator
    })
};