const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');
const compression = require('compression');
const helmet = require('helmet');

const createApp = require('./app');

function addExitSignals(app, server) {
    const exitSignals = ['SIGINT', 'SIGTERM', 'SIGQUIT'];

    exitSignals.forEach(signal => {
        process.on(signal, () => {
            server.close(err => {
                if (err) {
                    process.exit(1);
                }

                app.database.close(() => {
                    process.exit(0);
                });
            });
        });
    });
}

(async () => {
    dotenv.config();

    const middlewares = [cors(), compression(), helmet(), express.json()];

    const app = await createApp(express, mongoose, middlewares);

    await app.database.connect();

    const server = app.listen(process.env.PORT, () => {
        console.log(`App is listening on port ${process.env.PORT}!`);
    });

    addExitSignals(app, server);
})();