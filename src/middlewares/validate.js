function validate(joiValidator) {
    return async (req, res, next) => {
        try {
            await joiValidator.validateAsync(req.body);
            return next();
        } catch (error) {
            return res.status(412).json({ message: 'Os valores informados não são válidos.', error: error.message });
        }
    };
}

module.exports = validate;
