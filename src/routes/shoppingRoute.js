const Purchase = require('../models/Purchase');

function addRoute(router, controllers, middlewares) {
    const { shoppingController } = controllers;
    const { validate } = middlewares;

    router.post('/compras', 
        validate(Purchase.joiValidator), 
    
        async (req, res, next) => {
            await shoppingController.buy(req, res, next);
        }
    );
};

module.exports = addRoute;