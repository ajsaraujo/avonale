const Payment = require('../models/Payment');

function addRoute(router, controllers, middlewares) {
    const { paymentController } = controllers;
    const { validate } = middlewares;

    router.post('/pagamento/compras', 
        validate(Payment.joiValidator),
        
        (req, res, next) => {
            paymentController.authorizePayment(req, res, next);
        }
    );
}

module.exports = addRoute;