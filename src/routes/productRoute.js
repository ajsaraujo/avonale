const Product = require('../models/Product');

function addRoute(router, controllers, middlewares) {
    const { productController } = controllers;
    const { validate } = middlewares;

    router.get('/produtos', async (req, res, next) => {
        await productController.getAll(req, res, next);
    });

    router.post('/produtos', validate(Product.joiValidator), async (req, res, next) => {
        await productController.create(req, res, next);
    });

    router.get('/produtos/:id', async (req, res, next) => {
        await productController.getById(req, res, next);
    });

    router.delete('/produtos/:id', async (req, res, next) => {
        await productController.delete(req, res, next);
    });
}

module.exports = addRoute;
